/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2024. MIT licensed.
 */$(document).ready(function () {
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
    $('body').toggleClass('modal-open');
  });
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  });

  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active');
});